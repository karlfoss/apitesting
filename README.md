* The goal of this project is for testing integrations of RESTAPIs

### What to make ###

* https://sv443.net/jokeapi/v2 
	- joke api (mobile interface, use learning for 'good' jokes, integrate into discord/slack)
* https://covid19api.com/
	- use for making graphs
* https://discordapp.com/developers/docs/topics/community-resources - https://discordapp.com/developers/docs/intro
	

### How do I get set up? ###

* Add summary of setup
* Initial Setup Guide: https://www.freecodecamp.org/news/an-awesome-guide-on-how-to-build-restful-apis-with-asp-net-core-87b818123e28/
* List of APIs we can integrate with for free: https://github.com/public-apis/public-apis

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Owners ###

* Authors: Karl Foss, Mark Finta