﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebAPIClient;
using JokeMaker.Models;

namespace JokeMaker.Controllers
{
    [Route("api/[Controller]")]
    [ApiController]
    public class JokesController : ControllerBase
    {
        // GET api/values
        [HttpGet]
        public async Task<ActionResult<string>> GetAsync()
        {
            WebApiClient jokeClient = new WebApiClient("https://sv443.net");
            Joke newJoke = await jokeClient.GetAsync<Joke>("/jokeapi/v2/joke/Any") as Joke;
            return newJoke.JsonSerialize();
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
