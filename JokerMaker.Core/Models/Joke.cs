﻿using Newtonsoft.Json;

namespace JokeMaker.Models
{
    public class Joke
    {
        public string Category { get; set; }
        //TODO: type could be an enum
        public string Type { get; set; }
        public string Setup { get; set; }
        public string Delivery { get; set; }
        public Flags Flags { get; set; }
        public int Id { get; set; }
        public bool Error { get; set; }

        public string JsonSerialize()
        {
            return JsonConvert.SerializeObject(this);
        }

        public Joke JsonDeserialize(string value)
        {
            return JsonConvert.DeserializeObject<Joke>(value);
        }
    }

    public class Flags
    {
        public bool Nsfw { get; set; }
        public bool Religious { get; set; }
        public bool Political { get; set; }
        public bool Racist { get; set; }
        public bool Sexist { get; set; }
    }
}
