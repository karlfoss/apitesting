﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace WebAPIClient
{
    public class WebApiClient
    {
        readonly HttpClient _client;
        public WebApiClient(string uri)
        {
            _client = new HttpClient();
            _client.BaseAddress = new Uri(uri);
            _client.DefaultRequestHeaders.Accept.Clear();
            _client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
        }

        /// <summary>
        /// Generic async GET call based on an additional path
        /// </summary>
        /// <TODO>
        /// Better Exception Handling
        /// </TODO>
        /// <typeparam name="T"></typeparam>
        /// <param name="path"></param>
        /// <returns></returns>
        public async Task<object> GetAsync<T>(string path) where T : class
        {
            object value = null;
            HttpResponseMessage response = await _client.GetAsync(path);
            if (response.IsSuccessStatusCode)
            {
                value = await response.Content.ReadAsAsync<T>();
            }
            return value;
        }
    }
}
